#pragma once

#define MAX_OBJ_COUNT 1000

#define HERO_ID 0

#define GRAVITY 9.8

#define TYPE_NORMAL 0
#define TYPE_BULLET	1
#define TYPE_ENEMYBULLET	2
#define TYPE_ENEMY	3

#define WALL	499
#define WALL_POS	5
#define MAX_PARTICLE	1000

#define START_SCENE	0
#define PLAY_SCENE	1
#define END_SCENE	2
#define DEATH_SCENE	3