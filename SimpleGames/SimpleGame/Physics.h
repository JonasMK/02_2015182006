#pragma once
#include "Object.h"

class Physics
{
public:
	Physics();
	~Physics();

	bool isCollide(Object* a, Object* b, int type=0);
	void processCollision(Object* a, Object* b);

private:
	bool BBCollsion(Object* a, Object* b);
};

