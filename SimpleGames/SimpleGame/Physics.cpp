#include "stdafx.h"
#include "Physics.h"

Physics::Physics()
{
}

Physics::~Physics()
{
}

bool Physics::isCollide(Object* a, Object* b, int type)
{
	switch (type)
	{
	case 0:
		//BBCollision
		return BBCollsion(a, b);
		break;
	default:
		break;
	}



	return false;
}

void Physics::processCollision(Object* a, Object* b)
{
	//obj A
	float vx, vy, vz;
	float mass;
	a->GetVel(&vx, &vy, &vz);
	a->GetMass(&mass);

	//obj b
	float vx1, vy1, vz1;
	float mass1;
	b->GetVel(&vx1, &vy1, &vz1);
	b->GetMass(&mass1);

	// calc A
	float vfx, vfy, vfz;
	vfx = ((mass - mass1) / (mass + mass1)) * vx
		+ ((2 * mass1) / (mass + mass1)) * vx1;
	vfy = ((mass - mass1) / (mass + mass1)) * vy
		+ ((2 * mass1) / (mass + mass1)) * vy1;
	vfz = ((mass - mass1) / (mass + mass1)) * vz
		+ ((2 * mass1) / (mass + mass1)) * vz1;

	// calc  B
	float vfx1, vfy1, vfz1;
	vfx1 = ((2 * mass) / (mass + mass1)) * vx
		+ ((mass1 - mass) / (mass + mass1)) * vx1;
	vfy1 = ((2 * mass) / (mass + mass1)) * vy
		+ ((mass1 - mass) / (mass + mass1)) * vy1;
	vfz1 = ((2 * mass) / (mass + mass1)) * vz
		+ ((mass1 - mass) / (mass + mass1)) * vz1;

	a->SetVel(vfx, vfy, vfz);
	b->SetVel(vfx1, vfy1, vfz1);

}


bool Physics::BBCollsion(Object* a, Object* b)
{
	float minX, minY, minZ;
	float maxX, maxY, maxZ;
	float minX1, minY1, minZ1;
	float maxX1, maxY1, maxZ1;

	float x, y, z;
	float sx, sy, sz;

	float x1, y1, z1;
	float sx1, sy1, sz1;

	a->GetPos(&x, &y, &z);
	a->GetVol(&sx, &sy, &sz);

	b->GetPos(&x1, &y1, &z1);
	b->GetVol(&sx1, &sy1, &sz1);

	minX = x - sx / 2; maxX = x + sx / 2;
	minY = y - sy / 2; maxY = y + sy / 2;
	minZ = z - sz / 2; maxZ = z + sz / 2;

	minX1 = x1 - sx1 / 2; maxX1 = x1 + sx1 / 2;
	minY1 = y1 - sy1 / 2; maxY1 = y1 + sy1 / 2;
	minZ1 = z1 - sz1 / 2; maxZ1 = z1 + sz1 / 2;

	if (minX > maxX1)	return false;
	if (maxX < minX1)	return false;

	if (minY > maxY1)	return false;
	if (maxY < minY1)	return false;

	if (minZ > maxZ1)	return false;
	if (maxZ < minZ1)	return false;

	return true;
}
