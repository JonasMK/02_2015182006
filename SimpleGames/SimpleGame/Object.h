#pragma once

enum
{
	OBJ_NORMAL,
	OBJ_BUlLET
};

class Object
{
public:
	Object();
	~Object();

	void SetPos(float x, float y, float z);
	void GetPos(float *x, float *y, float *z);

	void SetVol(float x, float y, float z);
	void GetVol(float *x, float *y, float *z);

	void SetVel(float x, float y, float z);
	void GetVel(float *x, float *y, float *z);

	void SetAcc(float x, float y, float z);
	void GetAcc(float *x, float *y, float *z);

	void SetColor(float r, float g, float b, float a);
	void GetColor(float *r, float *g, float *b, float *a);

	void SetMass(float mass);
	void GetMass(float *mass);

	void SetFricCoef(float coef);
	void GetFricCoef(float *coef);

	void SetTex(int type);
	void GetTex(int *type);

	void AddForce(float x, float y, float z, float elapsedTime);

	void SetType(int id);
	void GetType(int* id);

	void SetHp(float hp);
	void GetHp(float* hp);
	void Damage(float dmg);

	void InitThis();
	void Update(float elpasedTimeInSecond);
	
	bool CanShootBullet();
	void ResetBulletCoolTime();

	void SetParentObj(Object* obj);
	bool IsAncestor(Object* obj);

	int iX = 0;
	int iY = 0;
	float distance = 5;

private:
	float	m_r, m_g, m_b, m_a;		// color
	float	m_posX, m_posY, m_posZ;	// position
	float	m_velX, m_velY, m_velZ;	// velocity
	float	m_accX, m_accY, m_accZ;	// acceleration
	float	m_volX, m_volY, m_volZ;	// volume
	float	m_mass;					// mass
	float	m_fricCoef;				// fricition coeffcient	
	int		m_type;					// obj_type
	float   m_hp;

	float m_remainingBulletCoolTime = 0.f;
	float m_BulletCoolTime = 0.2f;

	int m_texID = -1;


	Object* m_parent = NULL;
};

