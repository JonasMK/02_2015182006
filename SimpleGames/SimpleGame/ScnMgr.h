#pragma once
#include "Globals.h"
#include "Renderer.h"
#include "Object.h"
#include "Physics.h"
#include "Sound.h"


class ScnMgr
{
public:
	ScnMgr();
	~ScnMgr();

	void RenderScene();

	int AddObject(float x, float y, float z,
		float sx, float sy, float sz,
		float vx, float vy, float vz,
		float r, float g, float b, float a,
		float mass, 
		float fricCoef,
		float hp,
		int type);
	void DeleteObject(int idx);

	void Update(float elpasedTimeInSecond);
	void DoGarbageCollection();

	

	void KeyDownInput(unsigned char key, int x, int y);
	void KeyUpInput(unsigned char key, int x, int y);
	void SpecialKeyDownInput(int key, int x, int y);
	void SpecialKeyUpInput(int key, int x, int y);


	void MonsterSpawner_First(int time);
	void MonsterSpawner_Second(int time);

	int SceneNumber = START_SCENE;
	int timer = 0;
	int spawntimer = 0;
	int second_stage_timer = 0;
	int killpoint = 0;

private:
	Renderer * m_Renderer = NULL;
	Object * m_Obj[MAX_OBJ_COUNT];
	Object * Ememy_Obj[MAX_OBJ_COUNT];
	Physics* m_Physics = NULL;
	Sound* m_Sound = NULL;

	int Texture_index[20];

	bool m_KeyW = false;
	bool m_KeyS = false;
	bool m_KeyA = false;
	bool m_KeyD = false;

	bool m_KeyUp = false;
	bool m_KeyDown = false;
	bool m_KeyLeft = false;
	bool m_KeyRight = false;
};

