#include "stdafx.h"
#include "ScnMgr.h"
#include "Dependencies\freeglut.h"
#include <random>
#include <ctime>

int g_testTexID = -1;
int g_testAniTex = -1;

int g_playbgm = -1;
int g_deathbgm = -1;
int g_winbgm = -1;
int g_hit = -1;

int g_ParticleObj = 0;
float g_Particle = -1;

ScnMgr::ScnMgr()
{
	srand((unsigned int)time(0));

	// Initialize Renderer
	m_Renderer = new Renderer(1000, 1000);
	if (!m_Renderer->IsInitialized())
	{
		std::cout << "Renderer could not be initialized.. \n";
	}

	// Initialize m_Obj
	for (int i = 0; i < MAX_OBJ_COUNT; i++)
	{
		m_Obj[i] = NULL;
	}


	g_ParticleObj = m_Renderer->CreateParticleObject(100, -10, -10,
		10, 10, 3, 3, 9, 9,
		-10, -10, 10, 10);

	// Initialize physics
	m_Physics = new Physics();
	m_Sound = new Sound();

	m_Obj[HERO_ID] = new Object();
	m_Obj[HERO_ID]->SetPos(-1, 0, 0);
	m_Obj[HERO_ID]->SetVel(0, 0, 0);
	m_Obj[HERO_ID]->SetVol(1, 1, 1);
	m_Obj[HERO_ID]->SetMass(10);
	m_Obj[HERO_ID]->SetColor(1, 1, 1, 1);
	m_Obj[HERO_ID]->SetFricCoef(0.5);
	m_Obj[HERO_ID]->SetHp(100);
	m_Obj[HERO_ID]->SetType(0);

	g_testTexID = m_Renderer->GenPngTexture("./Textures/Inuyasha.png");
	g_testAniTex = m_Renderer->GenPngTexture("./Textures/Ani.png");

	g_playbgm = m_Sound->CreateBGSound("./Sound/02_Omen.mp3");
	g_deathbgm = m_Sound->CreateShortSound("./Sound/died.mp3");
	g_winbgm = m_Sound->CreateShortSound("./Sound/win.mp3");
	g_hit = m_Sound->CreateShortSound("./Sound/hit.mp3");

	Texture_index[TYPE_BULLET] = m_Renderer->GenPngTexture("./Textures/player_bullet.png");
	Texture_index[0] = m_Renderer->GenPngTexture("./Textures/Sprite.png");
	Texture_index[2] = m_Renderer->GenPngTexture("./Textures/startscene.png");
	Texture_index[3] = m_Renderer->GenPngTexture("./Textures/light.png");
	Texture_index[4] = m_Renderer->GenPngTexture("./Textures/Ground.png");
	Texture_index[5] = m_Renderer->GenPngTexture("./Textures/Ghost.png");
	Texture_index[6] = m_Renderer->GenPngTexture("./Textures/ghost_bl.png");
	Texture_index[7] = m_Renderer->GenPngTexture("./Textures/death.png");
	Texture_index[8] = m_Renderer->GenPngTexture("./Textures/youwin.png");


	m_Obj[HERO_ID]->SetTex(Texture_index[0]);


	m_Obj[900] = new Object();
	m_Obj[900]->SetPos(0, 0, 0);
	m_Obj[900]->SetVel(0, 0, 0);
	m_Obj[900]->SetVol(8, 8, 8);
	m_Obj[900]->SetMass(1);
	m_Obj[900]->SetColor(1, 1, 1, 1);
	m_Obj[900]->SetFricCoef(0.3);
	m_Obj[900]->SetHp(10);

	//m_Obj[i]->SetTex(g_testTexID);
	m_Obj[900]->SetTex(Texture_index[0]);
	//Add test obstacles
	//int temp = AddObject(1, 0, 0,
	//	0.5, 0.5, 0.5,
	//	0, 0, 0,
	//	1, 1, 1, 1,
	//	10, 0.7, 10, TYPE_NORMAL);

	//m_Obj[temp]->SetTex(g_testTexID);

}


ScnMgr::~ScnMgr()
{
}

void ScnMgr::RenderScene()
{
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glClearColor(0.0f, 0.3f, 0.3f, 1.0f);

	// Renderer Test
	if(SceneNumber == START_SCENE)
	{
		if(timer <= 90)
			m_Renderer->DrawTextureRect(0, -500, 0, 1000, 1000, 1, 1, 1, 1, timer / 90.f, Texture_index[2]);
		else
			m_Renderer->DrawTextureRect(0, -500, 0, 1000, 1000, 1, 1, 1, 1, (180 - timer) / 90.f, Texture_index[2]);

	}
	else if (SceneNumber == PLAY_SCENE)
	{
		m_Renderer->DrawTextureRect(0, 500, -1000, 1000, 1000, 0.3, 0.3, 0.3, 1, 1, Texture_index[4]);

		if (killpoint <= 30)
		{
			m_Renderer->DrawSolidRectGauge(
				450, 450, 0,
				0, 10 / 2.f, 0,
				100, 5, 0,
				1, 0, 0, 1,
				killpoint * 3.3f);
		}
		else
		{
			m_Renderer->DrawSolidRectGauge(
				450, 450, 0,
				0, 10 / 2.f, 0,
				100, 5, 0,
				1, 1, 0, 1,
				killpoint * 2.f);
		}

		for (int i = MAX_OBJ_COUNT-1; i >= 0; --i)
		{
			if (m_Obj[i] != NULL)
			{
				float x, y, z;
				float sx, sy, sz;
				float r, g, b, a;
				int texID;
				int type;

				m_Obj[i]->GetPos(&x, &y, &z);
				x = 100.f * x;  // convert to pixel size
				y = 100.f * y;
				z = 100.f * z;
				m_Obj[i]->GetVol(&sx, &sy, &sz);
				sx = 100.f * sx;  // convert to pixel size
				sy = 100.f * sy;
				sz = 100.f * sz;
				m_Obj[i]->GetColor(&r, &g, &b, &a);

				m_Obj[i]->GetTex(&texID);
				/*m_Renderer->DrawSolidRect(x, y, z, sx, r, g, b, a);*/
				float hp;
				m_Obj[i]->GetHp(&hp);

				m_Obj[i]->GetType(&type);
				if (type != TYPE_BULLET && type != TYPE_ENEMY)
				{
					m_Renderer->DrawSolidRectGauge(
						x, y, z,
						0, sy / 2.f, 0,
						sx, 5, 0,
						1, 0, 0, 1,
						hp * 1);
				}

				m_Obj[HERO_ID]->iX = (m_Obj[HERO_ID]->iX + 1) % 900;

				if (i == HERO_ID)
				{
					
					m_Renderer->DrawTextureRectAnim(
						x, y, z,
						sx, sy, sz,
						r, g, b, a,
						texID,
						9, 4,
						m_Obj[HERO_ID]->iX*0.01, m_Obj[HERO_ID]->iY);

					m_Renderer->DrawTextureRect(x, y-100, 50, sx*2, sy*2, sz, r, g, b, 0.5, Texture_index[3], false);

				}
				if(type == TYPE_ENEMY)
				{
					if (m_Obj[i]->distance < 5.f)
					{//
						m_Renderer->DrawTextureRect(x, y, z, sx, sy, sz, r, g, b, (3.f - m_Obj[i]->distance) / 3.f, texID);
						m_Obj[i]->distance += 0.05f;
					}

				}
				if (type == TYPE_BULLET)
				{
					float vx, vy, vz;
					m_Obj[i]->GetVel(&vx, &vy, &vz);
					float vSize = sqrtf(vx * vx + vy * vy + vz * vz);
					m_Renderer->DrawParticle(
						g_ParticleObj,
						x, y, z,
						1,
						1, 1, 1, 0.1,
						0, 0,
						Texture_index[3],
						1.f,
						vSize/3);
				}

			}
		}
	}
	else if (SceneNumber == DEATH_SCENE)
	{
		if (timer <= 181)
			m_Renderer->DrawTextureRect(0, -500, 0, 1000, 1000, 1, 1, 1, 1, timer / 180.f, Texture_index[7]);
	 }
	else if (SceneNumber == END_SCENE)
	{
		if (timer <= 181)
			m_Renderer->DrawTextureRect(0, -500, 0, 1000, 1000, 1, 1, 1, 1, timer / 180.f, Texture_index[8]);
	}
}


void ScnMgr::KeyDownInput(unsigned char key, int x, int y)
{
	if (key == 'w' || key == 'W')
	{
		m_KeyW = true;
	}
	else if (key == 'a' || key == 'A')
	{
		m_KeyA = true;
	}
	else if (key == 's' || key == 'S')
	{
		m_KeyS = true;
	}
	else if (key == 'd' || key == 'D')
	{
		m_KeyD = true;
	}
}

void ScnMgr::KeyUpInput(unsigned char key, int x, int y)
{
	if (key == 'w' || key == 'W')
	{
		m_KeyW = false;
	}
	else if (key == 'a' || key == 'A')
	{
		m_KeyA = false;
	}
	else if (key == 's' || key == 'S')
	{
		m_KeyS = false;
	}
	else if (key == 'd' || key == 'D')
	{
		m_KeyD = false;
	}
}

void ScnMgr::SpecialKeyDownInput(int key, int x, int y)
{
	if (key == GLUT_KEY_UP)
	{
		m_KeyUp = true;
	}
	else if (key == GLUT_KEY_DOWN)
	{
		m_KeyDown = true;
	}
	else if (key == GLUT_KEY_LEFT)
	{
		m_KeyLeft = true;
	}
	else if (key == GLUT_KEY_RIGHT)
	{
		m_KeyRight = true;
	}
}

void ScnMgr::SpecialKeyUpInput(int key, int x, int y)
{
	if (key == GLUT_KEY_UP)
	{
		m_KeyUp = false;
	}
	else if (key == GLUT_KEY_DOWN)
	{
		m_KeyDown = false;
	}
	else if (key == GLUT_KEY_LEFT)
	{
		m_KeyLeft = false;
	}
	else if (key == GLUT_KEY_RIGHT)
	{
		m_KeyRight = false;
	}
}

void ScnMgr::MonsterSpawner_First(int time)
{
	spawntimer += 1;
	if(spawntimer >= time)
	for (int i = 1; i < MAX_OBJ_COUNT; ++i)
	{
		if (m_Obj[i] == NULL)
		{
			int x = rand() % 11;
			int y = rand() % 11;
			float fx, fy;
			fx = x;
			fy = y;

			float px, py, pz;
			m_Obj[HERO_ID]->GetPos(&px, &py, &pz);

			float distance = sqrtf((x - px)*(x - px) + (y - py)*(y - py));

			if (x == 0 || x == 10)
			{
				m_Obj[i] = new Object();
				m_Obj[i]->SetPos((fx-5.f), y - 6.f, 0);
				if (fx == 0)
					m_Obj[i]->SetVel(3, 0, 0);
				else
					m_Obj[i]->SetVel(-3, 0, 0);
				m_Obj[i]->SetVol(1, 1, 1);
				m_Obj[i]->SetMass(10);
				m_Obj[i]->SetColor(1, 1, 1, 1);
				m_Obj[i]->SetFricCoef(0.001);
				m_Obj[i]->SetHp(3);

				m_Obj[i]->SetType(TYPE_ENEMY);

				m_Obj[i]->SetTex(Texture_index[5]);

				spawntimer = 0;
			}
			else if ((y == 0 || y == 10))
			{
				m_Obj[i] = new Object();
				
				m_Obj[i]->SetPos(x - 5.f, y - 6.f, 0);
				if(y == 0)
				m_Obj[i]->SetVel(0, 3, 0);
				else
					m_Obj[i]->SetVel(0, -3, 0);
				m_Obj[i]->SetVol(1, 1, 1);
				m_Obj[i]->SetMass(10);
				m_Obj[i]->SetColor(1, 1, 1, 1);
				m_Obj[i]->SetFricCoef(0.001);
				m_Obj[i]->SetHp(3);

				m_Obj[i]->SetType(TYPE_ENEMY);

				m_Obj[i]->SetTex(Texture_index[5]);

				spawntimer = 0;
			}



			//m_Obj[i]->SetTex(g_testTexID);
			break;
		}
	}
}

void ScnMgr::MonsterSpawner_Second(int time)
{
	spawntimer += 1;
	second_stage_timer += 1;
	MonsterSpawner_First(time);
	if (second_stage_timer >= time * 3)
		for (int i = 1; i < MAX_OBJ_COUNT; ++i)
		{
			if (m_Obj[i] == NULL)
			{
				int x = rand() % 11;
				int y = rand() % 11;
				float fx, fy;
				fx = x - 5.f;
				fy = y -5.f;

				float px, py, pz;
				m_Obj[HERO_ID]->GetPos(&px, &py, &pz);

				float distance = sqrtf((px - fx)*(px - fx) + (py - fy)*(py - fy));
				(fx - px) / distance;


				if (x == 0 || x == 10)
				{
					m_Obj[i] = new Object();
					m_Obj[i]->SetPos(fx, fy, 0);
					m_Obj[i]->SetVel((px - fx) / distance * 5, (py - fy) / distance * 5, 0);
					m_Obj[i]->SetVol(1, 1, 1);
					m_Obj[i]->SetMass(10);
					m_Obj[i]->SetColor(1, 1, 1, 1);
					m_Obj[i]->SetFricCoef(0.001);
					m_Obj[i]->SetHp(3);

					m_Obj[i]->SetType(TYPE_ENEMY);

					m_Obj[i]->SetTex(Texture_index[6]);

					second_stage_timer = 0;
				}
				else if ((y == 0 || y == 10))
				{
					m_Obj[i] = new Object();

					m_Obj[i]->SetPos(fx, fy, 0);
					m_Obj[i]->SetVel((px - fx) / distance * 5, (py - fy) / distance * 5, 0);
					m_Obj[i]->SetVol(1, 1, 1);
					m_Obj[i]->SetMass(10);
					m_Obj[i]->SetColor(1, 1, 1, 1);
					m_Obj[i]->SetFricCoef(0.001);
					m_Obj[i]->SetHp(3);

					m_Obj[i]->SetType(TYPE_ENEMY);

					m_Obj[i]->SetTex(Texture_index[6]);

					second_stage_timer = 0;
				}

				//m_Obj[i]->SetTex(g_testTexID);
				break;
			}
		}
}

int ScnMgr::AddObject(float x, float y, float z,
	float sx, float sy, float sz,
	float vx, float vy, float vz,
	float r, float g, float b, float a,
	float mass,
	float fricCoef,
	float hp,
	int type)
{
	// Search Empty slot idx
	int idx = -1;
	for (int i = 0; i < MAX_OBJ_COUNT; i++)
	{
		if (m_Obj[i] == NULL)
		{
			idx = i;
			break;
		}
	}

	if (idx == -1)
	{
		std::cout << "No more empty slot" << std::endl;
		return -1;
	}

	m_Obj[idx] = new Object();
	m_Obj[idx]->SetPos(x, y, z );
	m_Obj[idx]->SetVel(vx, vy, vz);
	m_Obj[idx]->SetVol(sx, sy, sz);
	m_Obj[idx]->SetMass(mass);
	m_Obj[idx]->SetFricCoef(fricCoef);
	m_Obj[idx]->SetColor(r, g, b, a);
	m_Obj[idx]->SetHp(hp);
	m_Obj[idx]->SetType(type);
	
	return idx;
}

void ScnMgr::DeleteObject(int idx)
{

	if (idx < 0)
	{
		std::cout << "Input idx is negative  : " << idx << std::endl;
		return;
	}

	if (idx >= MAX_OBJ_COUNT)
	{
		std::cout << "m_Obj[" << idx << "] is NULL" << std::endl;

		return;
	}

	if (m_Obj[idx] == NULL)
	{
		std::cout << "m_Obj[" << idx << "] is NULL" << std::endl;
		return;
 	}
	delete m_Obj[idx];
	m_Obj[idx] = NULL;
}


void ScnMgr::Update(float elapsedTime)
{
	if (SceneNumber == START_SCENE)
	{
		//m_Obj[900]->SetColor(1, 1, 1, (180 - timer) / 180);

		if (timer >= 180)
		{
			SceneNumber = PLAY_SCENE;
			m_Obj[900] = NULL;
			m_Sound->PlayBGSound(g_playbgm, true, 0.5f);
		}
		timer += 1;
	}
	else if (SceneNumber == PLAY_SCENE)
	{
		float fx = 0;
		float fy = 0;
		float fz = 0;
		float fAmount = 100.f;

		m_Obj[HERO_ID]->GetPos(&fx, &fy, &fz);
		if (fx <= -WALL_POS)
		{
			m_Obj[HERO_ID]->SetPos(-WALL * 0.01, fy, fz);
			float x, y, z;
			x = y = z = 0.f;
			m_Obj[HERO_ID]->GetVel(&x, &y, &z);
			x = 0;
			m_Obj[HERO_ID]->SetVel(x, y, z);
		}
		if (fx >= WALL_POS)
		{
			m_Obj[HERO_ID]->SetPos(WALL * 0.01, fy, fz);
			float x, y, z;
			x = y = z = 0.f;
			m_Obj[HERO_ID]->GetVel(&x, &y, &z);
			x = 0;
			m_Obj[HERO_ID]->SetVel(x, y, z);
		}
		if (fy >= WALL_POS - 0.5f)
		{
			m_Obj[HERO_ID]->SetPos(fx, (WALL - 50.f) * 0.01, fz);
			float x, y, z;
			x = y = z = 0.f;
			m_Obj[HERO_ID]->GetVel(&x, &y, &z);
			y = 0;
			m_Obj[HERO_ID]->SetVel(x, y, z);
		}
		if (fy <= -WALL_POS)
		{
			m_Obj[HERO_ID]->SetPos(fx, -WALL * 0.01, fz);
			float x, y, z;
			x = y = z = 0.f;
			m_Obj[HERO_ID]->GetVel(&x, &y, &z);
			y = 0;
			m_Obj[HERO_ID]->SetVel(x, y, z);
		}

		fx = 0;
		fy = 0;
		fz = 0;

		if (m_KeyW) {
			fy += fAmount;
			m_Obj[HERO_ID]->iY = 0;
		}
		if (m_KeyS) {
			fy -= fAmount;
			m_Obj[HERO_ID]->iY = 2;
		}
		if (m_KeyA) {
			fx -= fAmount;
			m_Obj[HERO_ID]->iY = 1;
		}
		if (m_KeyD) {
			fx += fAmount;
			m_Obj[HERO_ID]->iY = 3;
		}

		float fsize = sqrtf(fx * fx + fy * fy);
		if (fsize > 0.f)
		{
			fx /= fsize;
			fy /= fsize;
			fx *= fAmount;
			fy *= fAmount;

			m_Obj[HERO_ID]->AddForce(fx, fy, fz, elapsedTime);
		}


		float x, y, z;
		x = y = z = 0.f;
		m_Obj[HERO_ID]->GetVel(&x, &y, &z);
		if (sqrtf(x * x + fy * y) <= 0.01f) {
			m_Obj[HERO_ID]->iX = -1;
		}


		float bulVX, bulVY, bulVZ;
		bulVX = bulVY = bulVZ = 0.f;

		if (m_KeyUp) bulVY += 1.f;
		if (m_KeyDown) bulVY -= 1.f;
		if (m_KeyRight) bulVX += 1.f;
		if (m_KeyLeft) bulVX -= 1.f;

		float bulVSize =
			sqrtf(bulVX * bulVX + bulVY * bulVY);

		if (bulVSize > 0.f && m_Obj[HERO_ID]->CanShootBullet())
		{
			float bulletSpeed = 10.f;
			bulVX = bulletSpeed * bulVX / bulVSize;
			bulVY = bulletSpeed * bulVY / bulVSize;

			float heroX, heroY, heroZ;
			float heroVX, heroVY, heroVZ;

			m_Obj[HERO_ID]->GetPos(&heroX, &heroY, &heroZ);
			m_Obj[HERO_ID]->GetVel(&heroVX, &heroVY, &heroVZ);

			bulVX = bulVX + heroVX;
			bulVY = bulVY + heroVY;
			bulVZ = bulVZ + heroVZ;

			int temp = AddObject(heroX, heroY+0.3, heroZ,
				0.1, 0.1, 0.1,
				bulVX, bulVY, bulVZ,
				1, 1, 1, 1,
				1.f,
				0.6f,
				1.f,
				TYPE_BULLET
			);

			m_Obj[temp]->SetParentObj(m_Obj[HERO_ID]);
			m_Obj[temp]->SetTex(Texture_index[TYPE_BULLET]);

			m_Obj[HERO_ID]->ResetBulletCoolTime();
		}

		//----------------------------------------------------------------------------

		float dx, dy, dz, ox, oy, oz;

		int type = -1;
		int type_1 = -1;

		for (int i = 0; i < MAX_OBJ_COUNT; ++i)
		{
			if (m_Obj[i] != NULL)
			{
				m_Obj[i]->GetType(&type);

				for (int j = 0; j < MAX_OBJ_COUNT; ++j)
				{
					if (m_Obj[j] != NULL)
					{
						m_Obj[j]->GetType(&type_1);
						if (type != TYPE_ENEMY && type_1 == TYPE_ENEMY)
						{
							m_Obj[i]->GetPos(&dx, &dy, &dz);
							m_Obj[j]->GetPos(&ox, &oy, &oz);

							float distance = sqrt((dx - ox)*(dx - ox) + (dy - oy)*(dy - oy));
							if (distance <= m_Obj[j]->distance)
								m_Obj[j]->distance = distance;
						}
					}
				}
			}
		}

		for (int source = 0; source < MAX_OBJ_COUNT; ++source)
		{
			for (int target = source + 1; target < MAX_OBJ_COUNT; ++target)
			{
				
				// if collide
				// then set color to RED
				if (m_Obj[source] != NULL
					&& m_Obj[target] != NULL)
				{
					m_Obj[target]->GetType(&type);
					m_Obj[source]->GetType(&type_1);
					if (type != type_1)
					{
						if (m_Physics->isCollide(
							m_Obj[source],
							m_Obj[target]))
						{
							//cout << "Collision : " << source << " , " << target << endl;
							if (!m_Obj[source]->IsAncestor(m_Obj[target])
								&& !m_Obj[target]->IsAncestor(m_Obj[source]))
							{
								if (source != HERO_ID)
								{
									m_Physics->processCollision(
										m_Obj[source],
										m_Obj[target]);
									float srcHP, dstHP;
									m_Obj[source]->GetHp(&srcHP);
									m_Obj[target]->GetHp(&dstHP);
									float fsrcHP = srcHP - dstHP;
									float fdstHP = dstHP - srcHP;
									m_Obj[source]->SetHp(fsrcHP);
									m_Obj[target]->SetHp(fdstHP);
								}
								else if(source == HERO_ID){
									m_Physics->processCollision(
										m_Obj[source],
										m_Obj[target]);
									float srcHP, dstHP;
									m_Obj[source]->GetHp(&srcHP);
									m_Obj[target]->GetHp(&dstHP);
									float fsrcHP = srcHP - 3;
									float fdstHP = 0;
									m_Obj[source]->SetHp(fsrcHP);
									m_Obj[target]->SetHp(fdstHP);
								}
								if (source == HERO_ID)
									m_Sound->PlayShortSound(g_hit, false, 0.8f);
							}
						}
					}
				}
			}
		}

		if(killpoint <= 30)
		MonsterSpawner_First(20);
		else
			MonsterSpawner_Second(40);

		if (killpoint == 60)
		{
			SceneNumber = END_SCENE;
			timer = 0;

			m_Sound->StopBGSound(g_playbgm);
			m_Sound->PlayShortSound(g_winbgm, false, 0.5f);
		}

		//---------------------------------------------------------------------------

		for (int i = 0; i < MAX_OBJ_COUNT; i++)
		{
			if (m_Obj[i] != NULL)
			{
				m_Obj[i]->Update(elapsedTime);
			}
		}
	}
	else if (SceneNumber == DEATH_SCENE)
	{
		//m_Obj[900]->SetColor(1, 1, 1, (180 - timer) / 180);

		if (timer <= 180)
		{
			timer += 1;
		}
	}
	else if (SceneNumber == END_SCENE)
	{
	//m_Obj[900]->SetColor(1, 1, 1, (180 - timer) / 180);

	if (timer <= 180)
	{
		timer += 1;
	}
	}

}

void ScnMgr::DoGarbageCollection()
{
	for (int i = 0; i < MAX_OBJ_COUNT; i++)
	{
		if (m_Obj[i] != NULL)
		{
			int type;
			m_Obj[i]->GetType(&type);
			if (type == TYPE_BULLET)
			{
				float vx, vy, vz;
				m_Obj[i]->GetVel(&vx, &vy, &vz);
				float vSize = sqrtf(vx * vx + vy * vy + vz * vz);

				if (vSize < FLT_EPSILON)
				{
					DeleteObject(i);
					continue;
				}
			}
			else if (type == TYPE_ENEMY)
			{
				float x, y, z;
				m_Obj[i]->GetPos(&x, &y, &z);
				if (x >= 6.f || x <= -6.f || y >= 6.f || y <= -6.f)
				{
					DeleteObject(i);

					continue;
				}
			}
			float hp;
			m_Obj[i]->GetHp(&hp);
			if (hp < FLT_EPSILON)
			{
				if (type == TYPE_ENEMY)
				{
					killpoint += 1;
					DeleteObject(i);
				}
				if (i == HERO_ID && SceneNumber == PLAY_SCENE)
				{
					if (hp <= 1)
					{
						SceneNumber = DEATH_SCENE;
						timer = 0;
						m_Sound->StopBGSound(g_playbgm);
						m_Sound->PlayShortSound(g_deathbgm, false, 0.8f);
					}
				}
			}

		}
	}
}


